package me.psawesome.configserverclient.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class ClientController {

    @Value("${supplier.name}")
    private String SUPPLIER_NAME;

    @GetMapping("/supplier/name")
    public String getSupplierName() {
        return this.SUPPLIER_NAME;
    }
}
